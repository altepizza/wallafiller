'''
Created on Nov, 13 2016
Author Kai

'''

import os
import telepot
import time
import logging
import configparser
import feedparser
import re
from datetime import datetime
from time import mktime
from wallabag_api.wallabag import Wallabag

myLogger = logging.getLogger(__name__)
handler = logging.StreamHandler()


myLogger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(module)s --> %(message)s')
logPath = '.'
fileName = 'log'
fileHandler = logging.FileHandler("{0}/{1}.log".format(logPath, fileName))
fileHandler.setFormatter(formatter)

handler.setFormatter(formatter)
myLogger.addHandler(handler)
myLogger.addHandler(fileHandler)

myLogger.info('Start')

myLogger.debug('Reading config + init')

module_dir = os.path.dirname(__file__)
cfg_full_path = os.path.join(module_dir, 'config.ini')
config = configparser.RawConfigParser()
config.read(cfg_full_path)
telegramToken = config.get('Telegram', 'Token')
telegramChatID = config.getint('Telegram', 'ChatId')
frequency = config.getfloat('RSS', 'CheckFrequencyInSec')
startLastRun = config.getfloat('RSS', 'lastrun')
keywords = config.get('RSS', 'keywords')

bot = telepot.Bot(telegramToken)
myLogger.debug('Config Start Time: '+ str(startLastRun))
pattern = re.compile('.*'+keywords+'.*', re.IGNORECASE)
url = config.get('RSS', 'url')
checkFrequencyInSec = config.getfloat('RSS', 'CheckFrequencyInSec')
params = {'username': config.get('wallabag', 'username'),
          'password': config.get('wallabag', 'password'),
          'client_id': config.get('wallabag', 'client_id'),
          'client_secret': config.get('wallabag', 'client_secret')}
my_host = config.get('wallabag', 'host')

myLogger.debug('Finished reading config + init')
lastRun = None
token = Wallabag.get_token(host=my_host, **params)
myLogger.debug('Wallabag Token: ' + token)
wall = Wallabag(host=my_host, client_secret='mysecret', client_id='myid', token=token)
global lastRun

d = feedparser.parse(url)

myLogger.debug(len(d))
myLogger.debug(d)
for post in d.entries:
    myLogger.debug('Processing: ' + post.title)
    if re.match(pattern, post.title):
            myLogger.debug('Match found: ' + post.title)
            dt = datetime.fromtimestamp(mktime(post.published_parsed))
            dt = time.mktime(dt.timetuple())
            myLogger.debug('Post Time: ' + str(dt))
            if dt > startLastRun:
                myLogger.info('Post is new - Uploading: ' + post.title)
                if (lastRun is None) or (dt > lastRun):
                    myLogger.debug('Update lastRun from ' + str(lastRun) + ' to ' + str(dt))
                    lastRun = dt
                wall.post_entries(url=post.link, title=post.title)
                text = "New Article\n" + post.title
                bot.sendMessage(telegramChatID, text)
if not lastRun is None:
    config.set('RSS', 'LastRun', lastRun)
    with open('config.ini', 'w') as f:
       config.write(f)
myLogger.info('End')
